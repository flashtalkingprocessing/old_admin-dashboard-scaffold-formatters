<?php namespace Flashtalking;

class ExportFormatter extends ListFormatterAbstract
{
    public function render()
    {

        $preparedata = $this->prepareData();

        $outstream = fopen('php://output', 'w');

        $this->sendHeaders();

        $headData = array(
            "Rows: " . $preparedata->count(),
            "Filter: " . searchFilter($this->search),
            "Order: " . $this->sort . ', ' . $this->sortDirection
        );

        foreach ($headData as $row)
        {
            fputcsv($outstream, array($row));
        }

        fputcsv($outstream, array_keys($this->elements));
        
        foreach ($preparedata as $row)
        {
            fputcsv($outstream, $row);
        }
        
        fclose($outstream);
        
        return '';
    }

    private function sendHeaders()
    {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=export.csv');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

    }
}