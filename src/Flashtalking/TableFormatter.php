<?php namespace Flashtalking;

use Fieldset\Html\ValidHtmlTag;

class TableFormatter extends ListFormatterAbstract
{
    protected $filterNotEquals = ['list' => false];

    private function glyphiconDirection($dir, $sort, $name) {

        if ($sort === $name) {
            $glyphDir = ($dir === "asc" ? 'arrow-up' : 'arrow-down');
        } else {
            $glyphDir = "sort";
        }

        return $glyphDir;
    }

    public function render()
    {
        $page = $this->prepareData();

        $searchQuery = $this->searchQuery();

        $elements = $this->filterElements();

        $string = '<div class="col-md-12">';

        $string .= '<div class="pagination-wrapper top">';

        $string .= '<p>Showing page ' . $page->currentPage() . ' of ' . number_format($page->lastPage()) . '</p>';

        $string .= '<form action="/'. request()->path() .'" method="GET">';

        foreach($this->search as $name => $value) {
            foreach ($value as $index => $input) {
                $string .= '<input type="hidden" name="search['.$name.']['.$index.']" value="'.$input.'"/>';
            }
        }

        if (count($this->beid) > 0) {
            $string .= '<input type="hidden" name="beid" value="'.$this->beid.'"/>';
            $string .= '<input type="hidden" name="table" value="'.$this->tableName.'"/>';
        }

        $string .= ' <div><p>Results per page:</p> <input class="form-control" type="text" name="results" value="'.$this->resultsPerPage.'" /> <input class="form-control" type="hidden" name="sort" value="'.$this->sort.'" /> <input class="form-control" type="hidden" name="dir" value="'.$this->sortDirection.'" /> <input type="submit" value="View" class="btn btn-primary" /> </div>'. $this->getLinks($page, $this->resultsPerPage, $this->sort, $this->sortDirection, $this->beid, $this->tableName) .' </form>';

        $string .= '</div> <div class="clearfix"></div> <div class="table-wrapper"><table class="table custom table-striped table-hover table-responsive table-condensed fixed-width">';

        $string .= '<tr>';

        foreach($elements as $name => $element) {

            $th = new \Fieldset\Html\ValidHtmlTag('th', array_get($element,'columnLabel', array_get($element,'label')));

            $nextDirection = 'sort='.$name;

            if($this->sort === $name) {
                $nextDirections = array('asc' => '', 'desc' => $nextDirection . '&dir=asc');

                $nextDirection =  $nextDirections[$this->sortDirection];
            }

            $tblsort = (count($this->beid) > 0) ? '&beid='.$this->beid.'&table='.$this->tableName: '';

            $sorting = '';

            if (array_get($element, 'sort') !== false) {
                $sorting = '<a href="/'. request()->path().'?'.$searchQuery.'results='.$this->resultsPerPage.'&'.$nextDirection. $tblsort.'" title="Sort"><i class="glyphicon glyphicon-'.$this->glyphiconDirection($this->sortDirection, $this->sort, $name).'"></i></a>';
            }

            $string .= $th
                ->setAttributes(array_get($element, 'th', array()))
                ->setAttribute("class", strtolower(str_replace(" ", "-", array_get($element, 'label'))))
                ->addText('<p>' . array_get($element, 'label') . '</p>' . $sorting)
                ->render();
        }

        $string .= '</tr>';

        foreach($page as $row) {
            $string .= '<tr>';

            $primaryKeys = array_only($row, $this->data->getPrimaryKey());

            $i = 0;

            foreach($elements as $name => $element) {

                $data = $raw = array_get($row,$name);

                if(array_get($element, 'type') === 'select' && is_array($arrayOptions = array_get($element, 'options')) && isset($arrayOptions[$row[$name]])) {
                    $data = $arrayOptions[$raw];
                }

                if(is_callable($callback = array_get($element, 'list'))) {
                    $data = $callback($raw, $element, $row, $primaryKeys);
                }

                $class = '';

                foreach(array_get($element, 'columnStatus', array()) as $status => $function) {
                    if($function($data, $raw)) {
                        $class .= ' ' . $status;
                    }
                }

                $label = array_get($element,'columnLabel', array_get($element,'label'));

                $i++ === 0 ? $class .= strtolower(str_replace(" ", "-", $label)) : $class .= ' table-toggle-cell ' . strtolower(str_replace(" ", "-", $label));

                $attrs = ['data-th' => $label] + array_get($element, 'td', []);

                isset($attrs['class']) and $class .= ' ' . $attrs['class'];

                $attrs['class'] = $class;

                $td = new ValidHtmlTag('td', $attrs);

                $div = new ValidHtmlTag('div', $data);

                $td->addTag($div);

                $string .= $td->render();
            }

            $string .= '</tr>';
        }
        $string .= '</table></div>';

        $string .= '<div class="pagination-wrapper bottom">';

        $string .= '<p>Showing page ' . $page->currentPage() . ' of ' . number_format($page->lastPage()) . '</p>';

        $string .= $page->render();

        $string .= '</div></div>';

        return $string;
    }
}
