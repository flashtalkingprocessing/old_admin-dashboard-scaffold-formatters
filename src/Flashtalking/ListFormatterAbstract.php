<?php namespace Flashtalking;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Scaffold\Formatter\TableFormatter;
use Scaffold\ScaffoldDataSet;

abstract class ListFormatterAbstract extends TableFormatter
{

    protected $resultsPerPageKey = 'results';

    protected $sortKey = 'sort';

    protected $dirKey = 'dir';

    protected $beidKey = 'beid';

    protected $tableKey = 'table';

    protected $sort;

    protected $search;

    protected $tableName;

    protected $beid;

    protected $sortDirection;

    protected $resultsPerPage;

    public function __construct(array $elements, ScaffoldDataSet $data = null, $search = array(), $sessionTable = null)
    {
        parent::__construct($elements, $data);

        //Results

        if (Input::get($this->resultsPerPageKey) || !Session::get($sessionTable.'.results'))
        {
            $this->resultsPerPage = Input::get($this->resultsPerPageKey, 100);
        }
        else
        {
            $this->resultsPerPage = Session::get($sessionTable.'.results');
        }

        //Sort Direction

        if (Input::get($this->dirKey) || !Session::get($sessionTable.'.dir'))
        {
            $this->sortDirection = Input::get($this->dirKey);
        }
        else
        {
            $this->sortDirection = Session::get($sessionTable.'.dir');
        }

        $this->sortDirection === 'asc' or $this->sortDirection = 'desc';

        //Sorted Column

        if ((Input::get($this->sortKey) || !Session::get($sessionTable.'.sort')) || (!Input::get($this->sortKey) && Input::get($this->resultsPerPageKey)))
        {
            $this->sort = Input::get($this->sortKey);
        }
        else
        {
            $this->sort = Session::get($sessionTable.'.sort');
        }

        $this->beid = Input::get($this->beidKey);
        $this->tableName = Input::get($this->tableKey);

        $this->search = $search;
    }

    public function getLinks($paginator, $results, $sort, $dir, $beid, $tableName)
    {

        return $paginator->appends(array($this->dirKey => $dir,'search' => $this->search, $this->resultsPerPageKey => $results, $this->sortKey => $sort, $this->beidKey => $beid, $this->tableKey => $tableName))->render();
    }

    public function searchQuery()
    {
        return http_build_query(array('search' => $this->search)) . '&';
    }


    protected function prepareData()
    {
        if ($this->sort && isset($this->elements[$this->sort]))
        {
            $this->data->orderBy($this->sort, $this->sortDirection);
        }

        $page = $this->data->paginate($this->resultsPerPage);

        return $page;
    }
}

