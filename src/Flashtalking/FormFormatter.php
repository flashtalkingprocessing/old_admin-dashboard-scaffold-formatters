<?php namespace Flashtalking;

class FormFormatter extends \Scaffold\Formatter\FormatterAbstract {

    protected $customFormAttributes = array('list', 'form', 'th', 'columnStatus', 'columnLabel');
    protected $filterNotEquals = array(
        'form' => false
    );

    protected $errors = array();

    public function setErrors(array $errors) {
        $this->errors = $errors;
    }

    protected function renderForm($data = array())
    {
        $elements = $this->filterElements();

        $i = 0;

        $fieldset = new \Fieldset\Fieldset();

        foreach ($elements as $name => $element) {

            if ($i++ % 2 === 0)
            {
                $parentDiv = new \Fieldset\Html\ValidHtmlTag('div');
                $parentDiv->setAttribute('class', 'row');
                $fieldset->addTag($parentDiv);
            }

            $element['name'] = $name;

            $field = new \Fieldset\InputElement(array_get($element, 'type', 'text'), array_except($element, $this->customFormAttributes));

            $field->setComposer(function($field, $input, $label) use ($name, $element, $data) {

                $div = $field->tag('div')->setAttribute('class', 'form-row floated-input');

                if(is_callable($callback = array_get($element,'form')))
                {
                    $response = $callback(array_get($data, $name), $div, $element, $data);

                    $div->addText($response);
                }
                else {
                    $div->addTag($input);
                }

                $div->addTag($label);


                if (isset($this->errors[$name])) {
                    $div->setAttribute('class', 'has-error');
                }

                $field->addTag($div);
            });

            $field->input()->setAttribute('class', 'form-control');

            $field->setAttribute('class', 'col-md-6 col-sm-6 ');

            $parentDiv->addTag($field);
        }

        $fieldset->populate($data);

        return $fieldset->render();
    }

    public function render()
    {
        $return = '';

        if($this->data)
        {
            foreach($this->data->get() as $data)
            {
                $return .= $this->renderForm($data);
            }
        }
        else
        {
            return $this->renderForm();
        }

        return $return;
    }

}
