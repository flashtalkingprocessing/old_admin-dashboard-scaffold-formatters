<?php namespace Flashtalking;

class SearchFormatter extends \Scaffold\Formatter\SearchFormatter
{
    protected $filterNotEquals = array(
        'form' => false,
        'search' => false
    );

    public function render()
    {

        $search = array('Ignore','='=>'Equal To (=)','<>'=>'Not Equal (<>)','<'=>'Less Than (<)','>'=>'Greater Than (>)','%LIKE%'=>'Contains','LIKE%'=>'Starts With','%LIKE'=>'Ends With');
        
        $searchString = '';
        
        foreach($search as $value => $label)
        {
            $searchString .= '<option value="'.$value.'">'.$label.'</option>';
        }
        $string = '';

        foreach($this->filterElements() as $name => $element)
        {

            if (array_get($element, 'type') != "heading") {

                $key = 'search[' . $name . ']';

                $string .= '<div class="col-md-6 form-group">' .
                    '<label>' . array_get($element, 'label') . '</label>' .
                    '<input type="hidden" name="' . $key . '[0]" value="' . $name . '"/>' .
                    '<input class="form-control pull-right" type="text" name="' . $key . '[2]"/>' .
                    '<div class="select-wrap">' .
                    '<select class="pull-right" name="' . $key . '[1]">' . $searchString . '</select>' .
                    '<div class="select-caret"></div>' .
                    '<div class="clearfix"></div></div>' .
                    '</div>';
            }
        }

        return $string;
    }
}

