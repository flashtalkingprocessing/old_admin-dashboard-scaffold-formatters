<?php namespace Flashtalking;

use Fieldset\Html\ValidHtmlTag;

class SpecialFormFormatter extends \Scaffold\Formatter\FormatterAbstract {

    protected $customFormAttributes = array('list', 'form', 'th', 'columnStatus', 'columnLabel', 'help', 'after', 'td');
    protected $filterNotEquals = array(
        'form' => false
    );

    protected $errors = array();

    public function setErrors(array $errors) {
        $this->errors = $errors;
    }

    protected function renderForm($data = array())
    {
        $elements = $this->filterElements();

        $i = 0;

        $fieldset = new \Fieldset\Fieldset();

        $inGroup = false;

        foreach ($elements as $name => $element) {

            if (array_get($element, 'type') == 'heading') {
                $inGroup = true;
                $parentDiv = new \Fieldset\Html\ValidHtmlTag('div');
                if (++$i % 2 === 0) {
                    $parentDiv->setAttribute('class', 'col-md-6 col-sm-6 group-wrap pull-right');
                } else {
                    $parentDiv->setAttribute('class', 'col-md-6 col-sm-6 group-wrap');
                }
                $fieldset->addTag($parentDiv);
                $heading = new \Fieldset\Html\ValidHtmlTag('p');
                $heading->setAttribute('class', 'group-title');
                $heading->addText(array_get($element, 'value'));
                $parentDiv->addTag($heading);
                continue;
            }

            if ($i % 2 == 0 && $inGroup == false)
            {
                $parentDiv = new \Fieldset\Html\ValidHtmlTag('div');
                $fieldset->addTag($parentDiv);
            }

            $element['name'] = $name;

            $field = new \Fieldset\InputElement(array_get($element, 'type', 'text'), array_except($element, $this->customFormAttributes));

            $field->setComposer(function($field, $input, $label) use ($name, $element, $data) {

                $div = $field->tag('div')->setAttribute('class', 'form-row');
                $div2 = $field->tag('div');

                $div2->addTag($div);

                if($after = array_get($element, 'measure'))
                {
                    $div->addTag(new ValidHtmlTag('span', array(
                        'class' => 'measure',
                        'name' => $after
                    )));
                }

                if(is_callable($callback = array_get($element,'formBuilder')))
                {
                    $input = $callback($input->getAttribute('value'), $div, $element, $data);
                }

                if(is_callable($callback = array_get($element,'form')))
                {
                    $input->setAttribute('value', $callback($input->getAttribute('value'), $div, $element, $data));
                }

                $div->addTag($input);
                if (array_get($element, 'type') == 'select') {
                    $div->addTag(new \Fieldset\Html\ValidHtmlTag('span', array(
                        'class' => 'select-caret'
                    )));
                }


                $label and $div->addTag($label);

                if (isset($this->errors[$name])) {
                    $div->setAttribute('class', 'has-error');
                }

                if($help = array_get($element, 'help'))
                {
                    $div->addTag(new ValidHtmlTag('span', array(
                        'class' => 'glyphicon glyphicon-question-sign tooltip-icon',
                        'data-toggle' => 'tooltip',
                        'data-placement' => 'top',
                        'data-original-title' => $help
                    )));
                }

                $field->addTag($div2);
            });

            if ($inGroup == false) {
                $field->setAttribute('class', 'col-md-6 col-sm-6 group-wrap no-header');
            } else {
                $inGroup or $field->setAttribute('class', 'col-md-6 col-sm-6');
            }



            $parentDiv->addTag($field);
        }

        $fieldset->populate($data);

        return $fieldset->render();
    }

    public function render()
    {
        $return = '';

        if($this->data)
        {
            foreach($this->data->get() as $data)
            {
                $return .= $this->renderForm($data);
            }
        }
        else
        {
            return $this->renderForm();
        }

        return $return;
    }

}
